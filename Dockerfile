FROM alpine:latest

RUN apk update \
	&& apk add bash go python3 openssl openssh git make \
	&& pip3 install awscli

# Install kubectl from pre-built binary
ARG KUBE_VERSION
ADD https://storage.googleapis.com/kubernetes-release/release/v${KUBE_VERSION}/bin/linux/amd64/kubectl /usr/bin/kubectl
RUN chmod a+x /usr/bin/kubectl

COPY /kube-ecr /usr/local/bin/kube-ecr

ENTRYPOINT ["/usr/local/bin/kube-ecr"]
